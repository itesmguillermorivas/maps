package com.example.semana7

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.semana7.databinding.ActivityMapViewBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapViewActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private lateinit var binding: ActivityMapViewBinding
    private lateinit var mapView : MapView
    private lateinit var map : GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mapView = binding.mapView

        mapView.onCreate(null)
        mapView.getMapAsync(this)


    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        val edificio = LatLng(20.737030, -103.454188)
        map.addMarker(MarkerOptions().position(edificio).title("Edificio de ingenerías"))
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(edificio, 18f))
        map.setOnMapClickListener(this)
    }

    override fun onMapClick(latLng: LatLng) {
        map.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("marcador creado dinámicamente")
                .alpha(0.5f)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
        )
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    public fun hacerToast(view: View?){
        Toast.makeText(this, getString(R.string.mapview_toast), Toast.LENGTH_SHORT).show()

        val intent = Intent(this, HttpActivity::class.java)
        startActivity(intent)
    }
}