package com.example.semana7

import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.LruCache
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class HttpActivity : AppCompatActivity() {

    private lateinit var stringRequest : StringRequest
    private lateinit var queue : RequestQueue

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_http)

        queue = Volley.newRequestQueue(this)
    }

    public fun requestTexto(view : View?){

        // aquí hacemos un request
        val url = "https://developer.android.com/training/volley/simple"
        stringRequest = StringRequest(
            Request.Method.GET,
            url,
            Response.Listener<String> {
                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            },
            Response.ErrorListener {
                Toast.makeText(this, it.toString(), Toast.LENGTH_SHORT).show()
            }
        )

        stringRequest.tag = TAG
        queue.add(stringRequest)

    }

    public fun requestJSON(view : View?){

        // JSON - JavaScript Object Notation
        // XML es el que usábamos anteriormente
        // comunmente usado en web
        // notación ligera para intercambio de información
        // expresado en texto plano (no es binario)
        // requiere un parser para ser utilizado en código

        var jsonTest = "{'nombre':'Juan', 'edad':20}"
        var jsonTest2 = "{'nombre':'Pedro', 'calificaciones':[80, 50, 40, 30]}"
        var jsonTest3 = "[{'nombre':'Juan', 'edad':20}, {'nombre':'Maria', 'edad':19}, {'nombre':'Pepe', 'edad':21}]"


        try{

            val objeto = JSONObject(jsonTest)
            Log.wtf("JSON", objeto.getString("nombre"))
            Log.wtf("JSON", objeto.getInt("edad").toString())

            val objeto2 = JSONObject(jsonTest2)
            val calificaciones : JSONArray = objeto2.getJSONArray("calificaciones")
            for(i in 0 until calificaciones.length()){

                Log.wtf("JSON", calificaciones[i].toString())
            }

            val objeto3 = JSONArray(jsonTest3)
            for(i in 0 until objeto3.length()){

                val actual = objeto3.getJSONObject(i)
                Log.wtf("JSON", actual.getString("nombre"))
                Log.wtf("JSON", actual.getInt("edad").toString())

            }




        } catch(e: JSONException){
            e.printStackTrace()
        }

        //Toast.makeText(this, "Entrando", Toast.LENGTH_SHORT).show()
        var url = "https://disease.sh/v3/covid-19/countries"
        val jsonArrayRequest = JsonArrayRequest(
            Request.Method.GET,
            url,
            null,
            Response.Listener { response ->
                Toast.makeText(this, "Response: $response", Toast.LENGTH_SHORT).show()
                for(i in 0 until response.length()){

                    val actual = response.getJSONObject(i)
                    val countryInfo = actual.getJSONObject("countryInfo")
                    Log.wtf("JSON", "${actual.getString("country")} : ${countryInfo.getString("iso3")}")
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Error: $error", Toast.LENGTH_SHORT).show()
            }
        )

        queue.add(jsonArrayRequest)

        // solicitar imagen para poner en network image view
        // lo primero es crear un image loader
        val imageLoader = ImageLoader(queue, object : ImageLoader.ImageCache {

            private val cache : LruCache<String, Bitmap> = LruCache(10)

            override fun getBitmap(url: String?): Bitmap? {
                return cache.get(url)
            }

            override fun putBitmap(url: String?, bitmap: Bitmap?) {
                cache.put(url, bitmap)
            }

        })

        url = "https://disease.sh/assets/img/flags/dz.png"
        val networkImageView = findViewById<NetworkImageView>(R.id.networkImageView)
        networkImageView.setImageUrl(url, imageLoader)

    }

    override fun onStop() {
        super.onStop()
        queue.cancelAll(TAG)
    }

    companion object {

        private const val TAG = "request"
    }
}