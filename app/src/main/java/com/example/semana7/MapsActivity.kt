package com.example.semana7

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.semana7.databinding.ActivityMapsBinding
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // ya vimos como se obtiene la ubicación del usuario con una capa de google maps
        // vamos a usar fused services

        // crear un objeto solicitud
        // var solicitud = LocationRequest()

        var solicitud = LocationRequest.create().apply {

            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = 10 * 1000
            fastestInterval = 2 * 1000
        }

        // creamos el location settings request builder
        var locationSettingsRequestBuilder = LocationSettingsRequest.Builder()
        locationSettingsRequestBuilder.addLocationRequest(solicitud)

        // obtenemos el location request
        var locationSettingsRequest = locationSettingsRequestBuilder.build()


        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {

            // ahora sí solicitar updates!
            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(
                solicitud,
                object : LocationCallback() {

                    override fun onLocationResult(p0: LocationResult) {
                        super.onLocationResult(p0)

                        Log.wtf("ACTUALIZACIÓN DE UBICACIÓN", p0.lastLocation.toString())
                    }
                },
                Looper.myLooper()
            )
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val edificio = LatLng(20.737030, -103.454188)
        mMap.addMarker(MarkerOptions().position(edificio).title("Edificio de ingenerías"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(edificio, 18f))
        mMap.setOnMapClickListener(this)

        habilitarMyLocation()
    }

    fun habilitarMyLocation() {

        // verificar explícitamente si tenemos permiso para utilizar una funcionalidad
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED){

            // si el permiso no está dado solicitarlo explícitamente
            val permisos = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
            requestPermissions(permisos, 0)
        } else {

            // si el permiso está dado entonces correr la línea que necesita
            mMap.isMyLocationEnabled = true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

            mMap.isMyLocationEnabled = true
        } else {

            Toast.makeText(this, "NO ME DIERON PERMISO :'(", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onMapClick(latLng: LatLng) {

        mMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("marcador creado dinámicamente")
                .alpha(0.5f)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
        )
    }

    fun changeActivity(view: View?){

        val intent = Intent(this, MapViewActivity::class.java)
        startActivity(intent)
    }
}